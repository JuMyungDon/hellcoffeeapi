package com.md.hellcoffeeapi.controller;

import com.md.hellcoffeeapi.entity.Member;
import com.md.hellcoffeeapi.model.hell.HellCreateRequest;
import com.md.hellcoffeeapi.model.hell.HellItem;
import com.md.hellcoffeeapi.model.hell.HellResponse;
import com.md.hellcoffeeapi.model.hell.HellTotalCostChangeRequest;
import com.md.hellcoffeeapi.model.member.MemberItem;
import com.md.hellcoffeeapi.service.HellService;
import com.md.hellcoffeeapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/hell")
public class HellController {
    private final MemberService memberService;
    private final HellService hellService;

    @PostMapping("/new/member-id/{memberId}")
    public String setHell(@PathVariable long memberId, @RequestBody HellCreateRequest request){
        Member member = memberService.getData(memberId);
        hellService.setHell(member, request);

        return "ok";
    }

    @GetMapping("/hells")
    public List<HellItem> getHells(){
        return hellService.getHells();
    }

    @GetMapping("/hell/{hellId}")
    public HellResponse getHell(long hellId){
        return hellService.getHell(hellId);
    }

    @PutMapping("/total-cost/{totalCostId}")
    public String putTotalCost(@PathVariable long totalCostId, @RequestBody HellTotalCostChangeRequest request){
        hellService.putHellTotalCost(totalCostId,request);

        return "ok";
    }

    @DeleteMapping("/del-hell/{hellId}")
    public String delHell(@PathVariable long hellId){
        hellService.delHell(hellId);

        return "ok";
    }
}
