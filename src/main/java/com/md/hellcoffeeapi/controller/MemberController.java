package com.md.hellcoffeeapi.controller;

import com.md.hellcoffeeapi.model.member.MemberCreateRequest;
import com.md.hellcoffeeapi.model.member.MemberItem;
import com.md.hellcoffeeapi.model.member.MemberResponse;
import com.md.hellcoffeeapi.model.member.MemberSpendingChangeRequest;
import com.md.hellcoffeeapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request){
        memberService.setMember(request);

        return "ok";
    }

    @GetMapping("/members")
    public List<MemberItem> getMembers(){
        return memberService.getMembers();
    }

    @GetMapping("/member/{memberId}")
    public MemberResponse getMember(long memberId){
        return memberService.getMember(memberId);
    }

    @PutMapping("/spending/{spendingId}")
    public String putMemberSpending(@PathVariable long spendingId, @RequestBody MemberSpendingChangeRequest request){
        memberService.putMemberSpending(spendingId, request);

        return "ok";
    }
}
