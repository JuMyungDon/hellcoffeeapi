package com.md.hellcoffeeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HellCoffeeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HellCoffeeApiApplication.class, args);
	}

}
