package com.md.hellcoffeeapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberSpendingChangeRequest {
    private Double spending;
}
