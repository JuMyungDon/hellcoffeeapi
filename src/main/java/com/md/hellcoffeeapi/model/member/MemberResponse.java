package com.md.hellcoffeeapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberResponse {
    private String member;
    private String phoneNumber;
    private Double spending;
    private Long totalPrize;
}
