package com.md.hellcoffeeapi.model.hell;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HellTotalCostChangeRequest {
    private Double totalCost;
}
