package com.md.hellcoffeeapi.model.hell;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HellCreateRequest {
    private Double totalCost;
    private LocalDate dateCost;
}
