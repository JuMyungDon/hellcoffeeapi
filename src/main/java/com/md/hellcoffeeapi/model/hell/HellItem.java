package com.md.hellcoffeeapi.model.hell;

import com.md.hellcoffeeapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HellItem {
    private Long memberID;
    private Double totalCost;
    private LocalDate dateCost;
}
