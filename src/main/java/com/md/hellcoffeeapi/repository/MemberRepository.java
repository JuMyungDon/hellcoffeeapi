package com.md.hellcoffeeapi.repository;

import com.md.hellcoffeeapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
