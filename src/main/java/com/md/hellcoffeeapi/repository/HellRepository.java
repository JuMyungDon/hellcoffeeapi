package com.md.hellcoffeeapi.repository;

import com.md.hellcoffeeapi.entity.Hell;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HellRepository extends JpaRepository<Hell, Long> {
}
