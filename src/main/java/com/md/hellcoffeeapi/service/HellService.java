package com.md.hellcoffeeapi.service;

import com.md.hellcoffeeapi.entity.Hell;
import com.md.hellcoffeeapi.entity.Member;
import com.md.hellcoffeeapi.model.hell.HellCreateRequest;
import com.md.hellcoffeeapi.model.hell.HellItem;
import com.md.hellcoffeeapi.model.hell.HellResponse;
import com.md.hellcoffeeapi.model.hell.HellTotalCostChangeRequest;
import com.md.hellcoffeeapi.repository.HellRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HellService {
    private final HellRepository hellRepository;

    public void setHell(Member member, HellCreateRequest request){
        Hell addData = new Hell();
        addData.setMember(member);
        addData.setTotalCost(request.getTotalCost());
        addData.setDateCost(request.getDateCost());

        hellRepository.save(addData);
    }

    public List<HellItem> getHells(){
        List<Hell> originList = hellRepository.findAll();

        List<HellItem> result = new LinkedList<>();

        for (Hell hell : originList){
            HellItem addItem = new HellItem();
            addItem.setMemberID(hell.getMember().getId());
            addItem.setTotalCost(hell.getTotalCost());
            addItem.setDateCost(hell.getDateCost());

            result.add(addItem);
        }

        return result;
    }

    public HellResponse getHell(long hellId){
        Hell originData = hellRepository.findById(hellId).orElseThrow();

        HellResponse response = new HellResponse();
        response.setMemberId(originData.getMember().getId());
        response.setTotalCost(originData.getTotalCost());
        response.setDateCost(originData.getDateCost());

        return response;
    }

    public void putHellTotalCost(long hellId, HellTotalCostChangeRequest request){
        Hell originData = hellRepository.findById(hellId).orElseThrow();
        originData.setTotalCost(originData.getTotalCost());

        hellRepository.save(originData);
    }

    public void delHell(long id){
        hellRepository.deleteById(id);
    }
}
