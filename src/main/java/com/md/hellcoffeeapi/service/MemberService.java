package com.md.hellcoffeeapi.service;

import com.md.hellcoffeeapi.entity.Member;
import com.md.hellcoffeeapi.model.member.MemberCreateRequest;
import com.md.hellcoffeeapi.model.member.MemberItem;
import com.md.hellcoffeeapi.model.member.MemberResponse;
import com.md.hellcoffeeapi.model.member.MemberSpendingChangeRequest;
import com.md.hellcoffeeapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setMember(request.getMember());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setSpending(request.getSpending());
        addData.setTotalPrize(request.getTotalPrize());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList){
            MemberItem addItem = new MemberItem();
            addItem.setMember(member.getMember());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setSpending(member.getSpending());
            addItem.setTotalPrize(member.getTotalPrize());

            result.add(addItem);
        }

        return result;
    }

    public MemberResponse getMember(long memberId){
        Member originData = memberRepository.findById(memberId).orElseThrow();

        MemberResponse response = new MemberResponse();
        response.setMember(originData.getMember());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setSpending(originData.getSpending());
        response.setTotalPrize(originData.getTotalPrize());

        return response;
    }

    public void putMemberSpending(long memberId, MemberSpendingChangeRequest request){
        Member originData = memberRepository.findById(memberId).orElseThrow();
        originData.setSpending(request.getSpending());

        memberRepository.save(originData);
    }
}
